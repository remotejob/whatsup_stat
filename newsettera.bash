


#sed -i 's/ €//g'   Sheet1.tsv 


cut -d$'\t' -f5,6,7,11,13,14  Sheet1.tsv >setera03.tsv

cut -d$'\t' -f19,20,21,25,27,28  Sheet1.tsv >>setera03.tsv

cut -d$'\t' -f33,34,35,39,43,42  Sheet1.tsv >>setera03.tsv

cut -d$'\t' -f47,48,49,53,55,56  Sheet1.tsv >>setera03.tsv

cut -d$'\t' -f61,62,63,67,69,70 Sheet1.tsv >>setera03.tsv



01.2018 comparison

cut -d$'\t' -f3,5,10,20  setera01_04.tsv  >setera01.tsv 

sed -i 's/ €//g' setera01.tsv
sed -i 's/,//g' setera01.tsv

create table setera01 (set_bno varchar(20),set_kpl int, set_min decimal(7,2), set_lask decimal(7,2), set_alku decimal(7,2), set_eur_min decimal(7,2));

LOAD DATA LOCAL INFILE '/exwindoz/home/juno/gowork/src/gitlab.com/remotejob/whatsup_stat/setera01.tsv' INTO TABLE setera01 IGNORE 1 LINES;

create table vip01 as select adphone as vip_bno,count(*) as vip_kpl, CONVERT(sum(billsec)/60, DECIMAL(5,1)) as vip_min, CONVERT(sum(income), DECIMAL(15,2)) as vip_lask from rellinkweb_development.calls where cdate >= '2018-01-01' and cdate <'2018-02-01' and provider !='CUUMA'  group by adphone;

update vip01 set vip_bno = REPLACE(vip_bno,'0700','358700');
update vip01 set vip_bno = REPLACE(vip_bno,'0600','358600');

create table setera_vs_krasava_right01 as select v.vip_bno,set_kpl,set_min, set_lask, vip_kpl, vip_min, vip_lask,  (vip_lask - set_lask) as debit from setera01 s RIGHT JOIN vip01 v on s.set_bno=v.vip_bno;

echo "select * from setera_vs_krasava_right01" | mysql -uroot -pasterisksql rellinkweb_development > setera_vs_sipvip_debit012018.tsv

awk -F '\t' '{sum += $8} END {print sum}' setera_vs_sipvip_debit012018.tsv


02.2018 comparison

cut -d$'\t' -f3,6,11,21  setera01_04.tsv  >setera02.tsv 

sed -i 's/ €//g' setera02.tsv
sed -i 's/,//g' setera02.tsv

create table setera02 (set_bno varchar(20),set_kpl int, set_min decimal(7,2), set_lask decimal(7,2), set_alku decimal(7,2), set_eur_min decimal(7,2));

LOAD DATA LOCAL INFILE '/exwindoz/home/juno/gowork/src/gitlab.com/remotejob/whatsup_stat/setera02.tsv' INTO TABLE setera02 IGNORE 1 LINES;

create table vip02 as select adphone as vip_bno,count(*) as vip_kpl, CONVERT(sum(billsec)/60, DECIMAL(5,1)) as vip_min, CONVERT(sum(income), DECIMAL(15,2)) as vip_lask from rellinkweb_development.calls where cdate >= '2018-02-01' and cdate <'2018-03-01' and provider !='CUUMA'  group by adphone;

update vip02 set vip_bno = REPLACE(vip_bno,'0700','358700');
update vip02 set vip_bno = REPLACE(vip_bno,'0600','358600');

create table setera_vs_krasava_right02 as select v.vip_bno,set_kpl,set_min, set_lask, vip_kpl, vip_min, vip_lask,  (vip_lask - set_lask) as debit from setera02 s RIGHT JOIN vip02 v on s.set_bno=v.vip_bno;

echo "select * from setera_vs_krasava_right02" | mysql -uroot -pasterisksql rellinkweb_development > setera_vs_sipvip_debit022018.tsv

awk -F '\t' '{sum += $8} END {print sum}' setera_vs_sipvip_debit022018.tsv


03.2018 comparison

cut -d$'\t' -f3,7,12,22  setera01_04.tsv  >setera03.tsv 

sed -i 's/ €//g' setera03.tsv
sed -i 's/,//g' setera03.tsv

create table setera03 (set_bno varchar(20),set_kpl int, set_min decimal(7,2), set_lask decimal(7,2), set_alku decimal(7,2), set_eur_min decimal(7,2));

LOAD DATA LOCAL INFILE '/exwindoz/home/juno/gowork/src/gitlab.com/remotejob/whatsup_stat/setera03.tsv' INTO TABLE setera03 IGNORE 1 LINES;

create table vip03 as select adphone as vip_bno,count(*) as vip_kpl, CONVERT(sum(billsec)/60, DECIMAL(5,1)) as vip_min, CONVERT(sum(income), DECIMAL(15,2)) as vip_lask from rellinkweb_development.calls where cdate >= '2018-03-01' and cdate <'2018-04-01' and provider !='CUUMA'  group by adphone;

update vip03 set vip_bno = REPLACE(vip_bno,'0700','358700');
update vip03 set vip_bno = REPLACE(vip_bno,'0600','358600');

create table setera_vs_krasava_right03 as select v.vip_bno,set_kpl,set_min, set_lask, vip_kpl, vip_min, vip_lask,  (vip_lask - set_lask) as debit from setera03 s RIGHT JOIN vip03 v on s.set_bno=v.vip_bno;

echo "select * from setera_vs_krasava_right03" | mysql -uroot -pasterisksql rellinkweb_development > setera_vs_sipvip_debit032018.tsv

awk -F '\t' '{sum += $8} END {print sum}' setera_vs_sipvip_debit032018.tsv


04.2018 comparison

cut -d$'\t' -f3,8,13,23  setera01_04.tsv  >setera04.tsv 

sed -i 's/ €//g' setera04.tsv
sed -i 's/,//g' setera04.tsv

create table setera04 (set_bno varchar(20),set_kpl int, set_min decimal(7,2), set_lask decimal(7,2), set_alku decimal(7,2), set_eur_min decimal(7,2));

LOAD DATA LOCAL INFILE '/exwindoz/home/juno/gowork/src/gitlab.com/remotejob/whatsup_stat/setera04.tsv' INTO TABLE setera04 IGNORE 1 LINES;

create table vip04 as select adphone as vip_bno,count(*) as vip_kpl, CONVERT(sum(billsec)/60, DECIMAL(5,1)) as vip_min, CONVERT(sum(income), DECIMAL(15,2)) as vip_lask from rellinkweb_development.calls where cdate >= '2018-04-01' and cdate <'2018-05-01' and provider !='CUUMA'  group by adphone;

update vip04 set vip_bno = REPLACE(vip_bno,'0700','358700');
update vip04 set vip_bno = REPLACE(vip_bno,'0600','358600');

create table setera_vs_krasava_right04 as select v.vip_bno,set_kpl,set_min, set_lask, vip_kpl, vip_min, vip_lask, (vip_lask - set_lask) as debit from setera04 s RIGHT JOIN vip04 v on s.set_bno=v.vip_bno;

echo "select * from setera_vs_krasava_right04" | mysql -uroot -pasterisksql rellinkweb_development > setera_vs_sipvip_debit042018.tsv

awk -F '\t' '{sum += $8} END {print sum}' setera_vs_sipvip_debit042018.tsv



---------------------
 update  setera04 set set_bno=REPLACE(set_bno,'358700','0700');
 update  setera04 set set_bno=REPLACE(set_bno,'358600','0600');

  echo "select number, price from phones where number in (select set_bno from setera04)" | mysql -uroot -pasterisksql rellinkweb_development > setera_ph_number2018.tsv




05.2018 comparison


cut -d$'\t' -f5,6,7,11,13,14  setera01_05.tsv > setera05.tsv
cut -d$'\t' -f19,20,21,25,27,28  setera01_05.tsv >> setera05.tsv
cut -d$'\t' -f33,34,35,39,41,42  setera01_05.tsv >> setera05.tsv
cut -d$'\t' -f47,48,49,53,55,56  setera01_05.tsv >> setera05.tsv
cut -d$'\t' -f61,62,63,67,69,70  setera01_05.tsv >> setera05.tsv

#TEST
cut -d$'\t' -f1,2,3,4,5 setera05.tsv


sed -i 's/ €//g' setera05.tsv
#sed -i 's/,//g' setera05.tsv

create table setera05(set_bno varchar(20),set_kpl int, set_min decimal(7,2), set_lask decimal(7,2), set_alku decimal(7,2), set_eur_min decimal(7,2));


LOAD DATA LOCAL INFILE '/exwindoz/home/juno/gowork/src/gitlab.com/remotejob/whatsup_stat/setera05.tsv' INTO TABLE setera05;

create table vip05 as select adphone as vip_bno,count(*) as vip_kpl, CONVERT(sum(billsec)/60, DECIMAL(5,1)) as vip_min, CONVERT(sum(income), DECIMAL(15,2)) as vip_lask from rellinkweb_development.calls where cdate >= '2018-05-01' and cdate <'2018-06-01' and provider !='CUUMA'  group by adphone;

update vip05 set vip_bno = REPLACE(vip_bno,'0700','358700');
update vip05 set vip_bno = REPLACE(vip_bno,'0600','358600');


create table setera_vs_sipvip_right05 as select v.vip_bno,set_kpl,set_min, set_lask,set_alku,set_eur_min,vip_kpl, vip_min, vip_lask, (vip_lask - set_lask) as debit from setera05 s RIGHT JOIN vip05 v on s.set_bno=v.vip_bno;

echo "select * from setera_vs_sipvip_right05 order by debit desc" | mysql -uroot -p$SQLPASS rellinkweb_development > setera_vs_sipvip_debit052018.tsv

awk -F '\t' '{sum += $10} END {print sum}' setera_vs_sipvip_debit052018.tsv




06.2018 comparison


cut -d$'\t' -f5,6,7,11,13,14  setera01_06.tsv > setera06.tsv
cut -d$'\t' -f19,20,21,25,27,28  setera01_06.tsv >> setera06.tsv
cut -d$'\t' -f33,34,35,39,41,42  setera01_06.tsv >> setera06.tsv
cut -d$'\t' -f47,48,49,53,55,56  setera01_06.tsv >> setera06.tsv
cut -d$'\t' -f61,62,63,67,69,70  setera01_06.tsv >> setera06.tsv

#TEST
cut -d$'\t' -f1,2,3,4,5 setera06.tsv


sed -i 's/ €//g' setera06.tsv
#sed -i 's/,//g' setera05.tsv

create table setera06(set_bno varchar(20),set_kpl int, set_min decimal(7,2), set_lask decimal(7,2), set_alku decimal(7,2), set_eur_min decimal(7,2));


LOAD DATA LOCAL INFILE '/exwindoz/home/juno/gowork/src/gitlab.com/remotejob/whatsup_stat/setera06.tsv' INTO TABLE setera06;

create table vip06 as select adphone as vip_bno,count(*) as vip_kpl, CONVERT(sum(billsec)/60, DECIMAL(5,1)) as vip_min, CONVERT(sum(income), DECIMAL(15,2)) as vip_lask from rellinkweb_development.calls where cdate >= '2018-06-01' and cdate <'2018-07-01' and provider !='CUUMA'  group by adphone;

update vip06 set vip_bno = REPLACE(vip_bno,'0700','358700');
update vip06 set vip_bno = REPLACE(vip_bno,'0600','358600');


create table setera_vs_sipvip_right06 as select v.vip_bno,set_kpl,set_min, set_lask,set_alku,set_eur_min,vip_kpl, vip_min, vip_lask, (vip_lask - set_lask) as debit from setera06 s RIGHT JOIN vip06 v on s.set_bno=v.vip_bno;

echo "select * from setera_vs_sipvip_right06 order by debit desc" | mysql -uroot -p$SQLPASS rellinkweb_development > setera_vs_sipvip_debit062018.tsv

awk -F '\t' '{sum += $10} END {print sum}' setera_vs_sipvip_debit062018.tsv







